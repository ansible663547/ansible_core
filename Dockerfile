FROM alpine:3.17.2

LABEL MAINTAINER="Alexandr Khramov" MAINTAINER.MAIL="khramov-aa@ic.ntcees.ru"

ENV PYTHONUNBUFFERED=1
ENV PATH="$PATH:/root/.local/bin/"
ENV ANSIBLE_HOST_KEY_CHECKING=False

RUN apk add --update --no-cache python3 git && \
    ln -sf python3 /usr/bin/python && \
    python3 -m ensurepip && \
    pip3 install --no-cache --upgrade pip setuptools hvac && \
    apk add --update --no-cache openssh sshpass && \
    apk add rsync && \
    python3 -m pip install --upgrade --user setuptools urllib3 ndg-httpsclient pyasn1 ansible molecule lxml
    
#RUN mkdir /ENTRYPOINT
#ADD ./entrypoint.sh /ENTRYPOINT
#
WORKDIR /APP
#
ENTRYPOINT /bin/sh