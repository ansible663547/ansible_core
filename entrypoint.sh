#!/bin/sh


if [[ ${ANSIBLE_PLAYBOOK_FILE} ]];
then
    if [[ ${ANSIBLE_INVENTORY} ]] ; 
    then
        ANSIBLE_INVENTORY_FILE=${ANSIBLE_INVENTORY_FILE:-"inventory"}
        ANSIBLE_REQUIREMENTS_FILE=${ANSIBLE_REQUIREMENTS_FILE:-"requirements.yml"}
        echo $ANSIBLE_INVENTORY_FILE
        git clone $ANSIBLE_INVENTORY /tmp/inventory
    else
        echo "Не задан адрес для inventory"
        exit 1
    fi
    if [ -e /tmp/inventory/requirements.yml ]
    then
        ansible-galaxy install -r /tmp/inventory/$ANSIBLE_REQUIREMENTS_FILE
    fi
else
    echo "Не указан файл playbook"
    exit 1
fi

echo $*
ls -la /APP

ansible-playbook -i /tmp/inventory/$ANSIBLE_INVENTORY_FILE --extra-vars "$*" /tmp/inventory/playbook/$ANSIBLE_PLAYBOOK_FILE