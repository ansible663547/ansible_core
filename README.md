# ansible_core

## Пример запуска

docker run --rm -t -v /home/gitlab-runner/.ssh/:/key -v ${PACH_ANSIBLE}:/APP -e command="ansible-playbook -i /APP/ansible/hosts  /APP/ansible/laybook.yaml" registry.gitlab.com/ups-ic/devops/ansible/core:latest

## Описание
-v /home/gitlab-runner/.ssh/:/key - монтируем директорию с клчами для доступа к хостам
-v ${PACH_ANSIBLE}:/APP - монтируем директорию с ролями и сопутствующими файлами для ansible
-e command="ansible-playbook -i /APP/ansible/hosts  /APP/ansible/laybook.yaml" - передаем основную команду для исполнения в контейнере

